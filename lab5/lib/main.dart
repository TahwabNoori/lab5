//Author: Tahwab Noori, Damian Salazar, Carlitos Carmona
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class ReviewChip extends StatefulWidget {
  ReviewChip({this.reviewText});
  final String reviewText;
  @override
  State<StatefulWidget> createState(){
    return ReviewState(
        reviewText: this.reviewText
    );
  }
}

class ReviewState extends State<ReviewChip> {
  ReviewState({this.reviewText});
  final String reviewText;
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    return InputChip(
      label: Text(reviewText, style: _isSelected ?
      TextStyle(color: Colors.white) : TextStyle(color: Colors.black)),
      backgroundColor: Colors.white60,
      checkmarkColor: Colors.white,
      elevation: 5,
      pressElevation: 10,
      selectedColor: Colors.cyan,
      selected: _isSelected,
      onSelected: (bool newValue) {
        setState(() {
          _isSelected = newValue;
        });
      },
    );
  }
}
class ReviewWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.cyan,
        body: Center(
          child: Card(

            child: Column(
              mainAxisSize: MainAxisSize.min,

              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.mode_edit, color: Colors.cyan,),
                  title: Text('Select some skills you may have.'),
                ),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ReviewChip(reviewText: "Proficient in C++"),
                      ReviewChip(reviewText: "Management Experience"),
                      ReviewChip(reviewText: "Embedded Experience"),
                      ReviewChip(reviewText: "Team-Oriented"),
                      ReviewChip(reviewText: "Mass-Market Production Experience"),
                      ReviewChip(reviewText: "Proficient in Git version control"),
                      ReviewChip(reviewText: "Proficiency in working with Jenkins Servers"),
                    ]
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    OutlineButton(
                      child: const Text('Submit'),
                      highlightColor: Colors.cyan,
                      highlightedBorderColor: Colors.white,
                      splashColor: Colors.cyanAccent,
                      onPressed: () {
                        Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Your profile has been updated.", style: TextStyle(fontSize: 20),),
                              backgroundColor: Colors.cyan,
                              duration: Duration(seconds: 2),
                            )
                        );
                      },
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ],
            ),
          ),
        )
    );
  }
}


class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.cyan,
        body: Center(
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,

              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.account_circle, color: Colors.yellow,),
                  title: Text('Profile'),
                ),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ReviewChip(reviewText: "Proficient in C++"),
                      ReviewChip(reviewText: "Management Experience"),
                      ReviewChip(reviewText: "Embedded Experience"),
                      ReviewChip(reviewText: "Team-Oriented"),
                      ReviewChip(reviewText: "Mass-Market Production Experience"),
                      ReviewChip(reviewText: "Proficient in Git version control"),
                      ReviewChip(reviewText: "Proficiency in working with Jenkins Servers"),
                    ]
                ),
              ],
            ),
          ),

        )
    );
  }

}
class TabsState extends State<Tabs> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return DefaultTabController(
        length: 2,
        child: new Scaffold(
          appBar: AppBar(
            title: TooltipText(
              text: "LinkedUp",
              tooltip: "Sign Up Now",
            ),
            //Text("LinkedUp"),
            backgroundColor: Colors.cyan,
            leading: Icon(Icons.people_outline),
            bottom: TabBar(
                tabs:
                [
                  Tab(icon: Icon(Icons.school),),
                  Tab(icon: Icon(Icons.home),),
                ]

            ),
          ),
          body: TabBarView(children:
          [
//             any widget can work very well here <3
            Center(
                child: ReviewWidget()
            ),
            Center(
                child: ProfilePage()
            ),
          ]),
        ));
  }

}

class TooltipText extends StatelessWidget {
  final String text;
  final String tooltip;

  TooltipText({Key key, this.tooltip, this.text});

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: tooltip,
      child: Text(text),
    );
  }
}



class Tabs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState

    return TabsState();
  }

}

class MyHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Tabs()
        )
    );
  }
}

/*


class ToolBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(children: <Widget>[
        Container(
          child: Image.asset('images/reptile.jpg', width: 800, height: 450, fit: BoxFit.cover)
      ),
    Container(child: Row(children: <Widget> [
      Container(
        margin: EdgeInsets.all(25),
        child: Tooltip(
          message: "Background",
          child: Icon(Icons.info, size:50, color: Colors.green[900])
        )),
      Container(
        margin: EdgeInsets.all(20),
        child: Tooltip(
          message: 'Diet',
          child: Icon(Icons.fastfood, size: 50, color: Colors.orange[900]),
    )),
      Container(
        margin: EdgeInsets.all(20),
        child: Tooltip(
          message: 'History',
          child: Icon(Icons.history, size: 50, color: Colors.blue[900]),
    )),
      Container(
        margin: EdgeInsets.all(10),
        child: Tooltip(
          message: 'Post',
          child: FlatButton(
            child: Icon(
              Icons.mobile_screen_share, size: 40, color: Colors.red),
          onPressed: () {
              Navigator.of(context).pop();
          },
        ))
        ),
       ]
      ),
      ),
    ])
    )
  }
}


 */



